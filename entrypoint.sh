#!/bin/bash

# TODO: check sleep time
SLEEP_TIME=${SLEEP_TIME:-60}
OAUTH2_DIR=/etc/fetchmail/oauth2
FETCHMAILRC_DIR=/etc/fetchmail/fetchmailrc

function print_debug { if [[ -n ${DEBUG} ]]; then echo "$1"; fi }

print_debug "
Config:
  * SLEEP_TIME: ${SLEEP_TIME}
  * OAUTH2_DIR: ${OAUTH2_DIR}
  * FETCHMAILRC_DIR: ${FETCHMAILRC_DIR}
"

if [ -z "$(ls -A ${OAUTH2_DIR}/)" ]; then
  echo "Oauth2 config directory (${OAUTH2_DIR}) is empty, exiting."
  exit 10
fi

while true; do
  for oauth2_config in ${OAUTH2_DIR}/*; do
    print_debug "Found oauth2 config: ${oauth2_config}"

    fetchmailrc_file=${FETCHMAILRC_DIR}/$(basename "${oauth2_config}")
    if [ ! -f "${fetchmailrc_file}" ]; then
      echo "Cannot read file: ${fetchmailrc_file}"
      exit 11
    fi

    # TODO: refresh the oauth2 token only if it's needed
    echo "Refreshing access token for ${oauth2_config}"
    fetchmail-oauth2.py -c "${oauth2_config}" --auto_refresh
    status=$?
    if [ $status -ne 0 ]; then
      echo "Failed to refresh access token. (return code ${status})"
      echo "Skipping fetchmail run for ${fetchmailrc_file}"
      continue
    fi
    sleep 3

    fetchmail_cmd="$@ -f ${fetchmailrc_file}"
    echo "Running fetchmail (${fetchmail_cmd})"
    $fetchmail_cmd
  done
  echo "Sleeping for ${SLEEP_TIME}..."
  sleep ${SLEEP_TIME}
done