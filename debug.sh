docker run --rm -ti \
	-v `pwd`/config/fetchmail:/etc/fetchmail \
	-v `pwd`/entrypoint.sh:/entrypoint.sh \
	-e DEBUG=1 \
	-e SLEEP_TIME=120 \
	--entrypoint /bin/bash \
	registry.gitlab.com/olegfiksel/fetchmail-oauth2:build-6afb99c5ac8d4b3c0884635a4905d45e7eeec13e
