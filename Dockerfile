FROM ubuntu:22.04 AS builder

ARG COMMIT=next
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get install -y build-essential git dh-autoreconf pkg-config libssh-dev bison flex

RUN git clone -b next https://gitlab.com/fetchmail/fetchmail.git && \
	cd fetchmail && git checkout ${COMMIT} && \
	git branch && \
	./autogen.sh && \
	./configure && \
	make && \
	make install && \
	cp -v contrib/fetchmail-oauth2.py /usr/local/bin/ && \
	chmod +x /usr/local/bin/fetchmail-oauth2.py

FROM ubuntu:22.04

RUN apt-get update && apt-get install -y python3 python-is-python3 ca-certificates && \
	apt-get clean -y

RUN useradd -m -u 1000 -g 100 fetchmail

COPY --from=builder /usr/local /usr/local
COPY obtain_refresh_token.sh /usr/local/bin/
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["fetchmail"]
USER fetchmail
