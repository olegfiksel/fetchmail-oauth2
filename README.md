# Fetchmail with OAuth2 support

This Docker-packaged version of fetchmail will run a **loop** over the following steps:
* Running `fetchmail-oauth2.py --auto_refresh -c file` on every file located in `/etc/fetchmail/oauth2/`
* Running fetchmail agains every config file in `/etc/fetchmail/fetchmailrc/`
* Sleeping for `SLEEP_TIME` seconds

# Setting up

The OAuth2 credentials and fetchmail configuration is mounted under `/etc/fetchmail`.

See [config-examples](./config-examples).

* Create dir structure
  * `mkdir -p config/fetchmail/{creds,oauth2}`
```
find config -type d
config
config/fetchmail
config/fetchmail/oauth2
config/fetchmail/fetchmailrc
config/fetchmail/creds
```

## Create a google app

1. Login to [Google API Console](https://console.developers.google.com/project/_/apiui/apis/library) using your google account credentials

![](./images/screenshot01.png)

2. Create a new project and select it

![](./images/screenshot02.png)

3. Select `OAuth consent screen` on the left sidebar and create an `External` consent screen

![](./images/screenshot03.png)

4. Add your google account as a test user
5. Select `Credentials` on the left sidebar and create a new credential (`+`) and select `OAuth client ID`.
    * Application type: `TVs and Limited Input Devices`
    * Note the `OAuth2 Client ID` and `OAuth Client Secret`

## Get refresh token

**Replace `<gmail.name>` with your googlemail login name or with any name to distinguish between the googlemail accounts**.

1. Create a oauth2 config file `./config/oauth2/<gmail.name>`
```
client_id=YOUR-ID-FROM-GOOGLE-API-CONSOLE
client_secret=YOUR-SECRET-FROM-GOOGLE-API-CONSOLE
refresh_token_file=/etc/fetchmail/creds/<gmail.name>_refresh_token
access_token_file=/etc/fetchmail/creds/<gmail.name>_access_token
max_age_sec=1900
```
2. Uncomment the entrypoint in `docker-compose.yml` and run the container
3. Exec into the container to run the bash
4. Get the first refresh tocken by running `obtain_refresh_token.sh /etc/fetchmail/oauth2/gmail_name` and following the instructions

## Configure fetchmail

1. Edit the the fetchmail's config `./config/fetchmail/fetchmailrc/gmail.name`
```
poll "imap.googlemail.com"
  protocol imap
  service 993
  auth oauthbearer username "<gmail.name>@googlemail.com"
  passwordfile "/etc/fetchmail/creds/<gmail.name>_access_token"
  sslmode wrapped sslprotocolversion TLS1.2
  smtphost mail.my-mail-server-domain/25
  smtpname my.user@my-mail-server-domain
```

2. fetchmailrc file (`./config/fetchmail/fetchmailrc/gmail.name`) must be mode `0700` and owner `1000` for fetchmail to read it. If it isn't fetchmail will terminate with error.

Source: http://mmogilvi.users.sourceforge.net/software/oauthbearer.html

# Running

* See [docker-compose.yml](/docker-compose.yml) in this repo as an example
* `docker-compose up`

# Environment variables

* `SLEEP_TIME`: Seconds between polls (default 60)
* `DEBUG`: display debugging info. Currently only for entrypoint script.

# Exit codes

* 10: No oauth2 configs found in (`/etc/fetchmail/oauth2/`)
* 11: Cannot read fetchmailrc file located in (`/etc/fetchmail/fetchmailrc/`)
